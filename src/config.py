import os
import sys

def get_config():
    current_dir = os.path.abspath('.')
    template_dir = current_dir + '/templates'
    return template_dir

if __name__ == '__main__':
    print(get_config())
