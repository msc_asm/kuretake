import tornado.ioloop
import tornado.web
from tornado import template
from config import get_config 

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        template_dir = get_config()
        loader = template.Loader(template_dir)
        self.render(template_dir + '/index.html')

def make_app():
    return tornado.web.Application([
        (r'/', MainHandler)
    ])

if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
